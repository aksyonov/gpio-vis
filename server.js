const socketio = require('socket.io')
const {Gpio} = require('onoff')

const io = socketio(4000)

function initByteInputs() {
  return [
    new Gpio(21, 'in', 'both'),
    new Gpio(20, 'in', 'both'),
    new Gpio(16, 'in', 'both'),
    new Gpio(12, 'in', 'both'),
    new Gpio(25, 'in', 'both'),
    new Gpio(24, 'in', 'both'),
    new Gpio(23, 'in', 'both'),
    new Gpio(18, 'in', 'both')
  ]
}

function readByte(inputs) {
  return parseInt(inputs.map(input => input.readSync()).join(''), 2)
}

function listenGpio() {
  const inputs = initByteInputs()

  function update() {
    io.emit('update', readByte(inputs))
  }

  io.on('connection', () => update())

  update()
  inputs.forEach(input => input.watch(update))
}

function listenArgumentResult() {
  const inputs = initByteInputs()
  let threshold = 0
  let interval
  
  const pulse = new Gpio(26, 'in', 'rising')

  function update() {
    io.emit('update', readByte(inputs))
  }

  pulse.watch(() => {
	if (threshold !== 0) clearInterval(interval)
	threshold = 10000
	const input = readByte(inputs)
	console.log('Got input value', input)
	interval = setInterval(() => {
	  const output = readByte(inputs)
	  if (input !== output && output) {
		console.log('Got output value', output)
		io.emit('result', {input, output})
		threshold = 0
		clearInterval(interval)
	  } else if (threshold === 0) {
		clearInterval(interval)
	  } else {
		threshold--
	  }
	}, 1)
  })
}

function generateSequence() {
  let value = 0

  function update() {
    io.emit('update', value)
    value = value === 255 ? 0 : value + 1
  }

  const interval = setInterval(update, 1000)

  return () => clearInterval(interval)
}

try {
  listenArgumentResult()
} catch (error) {
  generateSequence()
}
