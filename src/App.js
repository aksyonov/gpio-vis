import React, { PureComponent } from 'react'
import './App.css'
import {socket} from './socket'
import schema from './schema.png'

class App extends PureComponent {
  constructor() {
    super()
    this.state = {value: 0, input: 0, output: 0}
  }

  componentWillMount() {
    // socket.on('update', value => this.setState({value}))
    socket.on('result', value => this.setState(value))
  }
  
  render() {
    const {value, input, output} = this.state
    const bin = toBin(value)

    return (
      <div className="app">
        <div className="codes" style={{opacity: value || input ? 1 : 0}}>
          <p>
            <strong>Двійковий код: </strong>
            <tt>{input ? toBin(input) : toBin(value)}</tt>
            <i> ({input || value})</i>
          </p>
          <p>
            <strong>Код Грея: </strong>
            <tt>{output || toGray(value)}</tt>
          </p>
        </div>
        <div className="schema">
          <img src={schema} />
        </div>
      </div>
    )
  }
}

function toBin(value) {
  return value.toString(2).padStart(8, '0')
}

function toGray(value) {
  return toBin(value ^ (value >> 1))
}

export default App
